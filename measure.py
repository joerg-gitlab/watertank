#!/usr/bin/env python3

import numpy as np
import serial
import math
import time
import signal
import logging
from influxdb import InfluxDBClient

# ---< some constants >-------------------------------------------------------------------------------------------------

# Tank data in meters
tank_sensor_distance = 0.2
tank_radius = 0.650
tank_length = 2.45

# for some reasons the tank data does not fit 100% with reality, so a factor is used to scale the result
adjustment_factor = 1.434778172666519

# read interval in seconds
sleep_time = 1

# ---< global vars >----------------------------------------------------------------------------------------------------

# measurement
measure_rawdata: int = 0  # distance return from sensor in millimeter
measure_fill_height: int = 0  # calculated fill level in millimeter
measure_fill_value: int = 0  # fill volume in liters


# ---< functions >------------------------------------------------------------------------------------------------------

# tank calculation
def calc_tank_data():
    """This calculates the tank fill value in liter"""

    global measure_fill_height
    global measure_fill_value

    # also calc fill height in millimeters based on distance and tank height
    measure_fill_height = int(max(((tank_radius * 2) + tank_sensor_distance) * 1000 - measure_rawdata, 0))

    # fill level in meter
    measure_fill_height_m = measure_fill_height / 1000

    # calc fill width based on fill height and tank radius
    measure_fill_width = 2 * math.sqrt(2 * tank_radius * measure_fill_height_m - measure_fill_height_m ** 2)

    # calc circle arc length based fill height and tank radius
    measure_arc_length = tank_radius * 2 * math.acos(1 - measure_fill_height_m / tank_radius)

    # calc side area based on tank radius, arc length, fill height and side radius
    measure_side_area = tank_radius * measure_arc_length / 2 - measure_fill_width * (
            tank_radius - measure_fill_height_m) / 2

    # finally calc volume capacity base on side area and tank length - !use adjustment factor!
    measure_fill_value = int(measure_side_area * tank_length * 1000 * adjustment_factor)


def read_sensor():
    """Turn on the sensor, discard num_discard values, read num_values values, determine the median and return as
    distance"""

    # consts
    num_discard = 4
    num_values = 11  # use an odd number to simple get the median value

    # Init array
    values = np.empty(num_values)

    try:
        # set rts to active measuring
        ser.rts = False

        ser.flushInput()

        # Read some value and discard them (The first readings may contain obsolete values)
        for i in range(0, num_discard):
            ser.read_until(b'\r')

        # read values
        for i in range(0, num_values):
            x: bytes = ser.read_until(b'\r')
            values[i] = int(x.lstrip(b'R'))

        # deactivate measuring
        # ser.rts = True

        # find median
        np.sort(values)
        rawdata = values[num_values >> 1]

        # debug
        print(rawdata, time.strftime('%H:%M:%S'))
    except:
        rawdata = 0

    return rawdata


def open_serial():
    print("open serial")
    return serial.Serial(
        port='/dev/tty.usbserial-FTADZ0HM',
        # port='/dev/tty.Bluetooth-Incoming-Port',
        # port='/dev/ttyUSB0',
        baudrate=9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1,
        rtscts=True
    )


def handler_stop_signals(signum, frame):
    global run
    run = False


# ---< main >-----------------------------------------------------------------------------------------------------------

# register signal handler
signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)

# init serial interface
ser = open_serial()

# if rtscts is set to false rts can be used to manually signal the RTS line. Worked only if I set rtscts = True by init
ser.rtscts = False
# ser.rts = True
ser.flushInput()

# init database
print("init Influx DB")
db: object = InfluxDBClient(host='localhost',
                            port=8086,
                            username='measure',
                            password='DUK9T2NeFvrMawTNaETm',
                            database='watertank')
db.switch_database('watertank')

# main loop
run: bool = True

print("running")
while run:
    t1 = time.time()

    # get a new value, 0 if sensor can not be read
    measure_rawdata = int(read_sensor())

    if measure_rawdata > 0:
        measure_rawdata = max(measure_rawdata, int(tank_sensor_distance * 1000))

        # calculate the data
        calc_tank_data()

        # and write into database
        pointValues = [
            {
                "measurement": "water",
                'fields': {
                    "rawdata": measure_rawdata,
                    "filllevel": measure_fill_height,
                    "liter": measure_fill_value
                }
            }
        ]

        db.write_points(pointValues)

    # sleep
    while run and (t1 + sleep_time > time.time()):
        time.sleep(0.2)

# cleanup
ser.flushInput()
ser.close()
db.close()
